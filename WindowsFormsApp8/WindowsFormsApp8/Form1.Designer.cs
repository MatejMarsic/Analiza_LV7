﻿namespace WindowsFormsApp8
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.o_pobjede_lbl = new System.Windows.Forms.Label();
            this.izjednaceno_lbl = new System.Windows.Forms.Label();
            this.x_pobjede_lbl = new System.Windows.Forms.Label();
            this.o_pobjede = new System.Windows.Forms.Label();
            this.izjednaceno = new System.Windows.Forms.Label();
            this.x_pobjede = new System.Windows.Forms.Label();
            this.newgame = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(16, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(396, 28);
            this.label1.TabIndex = 0;
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button5.Location = new System.Drawing.Point(139, 141);
            this.button5.Margin = new System.Windows.Forms.Padding(4);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(123, 112);
            this.button5.TabIndex = 5;
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button_Click);
            this.button5.MouseLeave += new System.EventHandler(this.button_MouseLeave);
            this.button5.MouseHover += new System.EventHandler(this.button_MouseHover);
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button6.Location = new System.Drawing.Point(270, 141);
            this.button6.Margin = new System.Windows.Forms.Padding(4);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(123, 112);
            this.button6.TabIndex = 6;
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button_Click);
            this.button6.MouseLeave += new System.EventHandler(this.button_MouseLeave);
            this.button6.MouseHover += new System.EventHandler(this.button_MouseHover);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button4.Location = new System.Drawing.Point(8, 141);
            this.button4.Margin = new System.Windows.Forms.Padding(4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(123, 112);
            this.button4.TabIndex = 4;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button_Click);
            this.button4.MouseLeave += new System.EventHandler(this.button_MouseLeave);
            this.button4.MouseHover += new System.EventHandler(this.button_MouseHover);
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button7.Location = new System.Drawing.Point(8, 21);
            this.button7.Margin = new System.Windows.Forms.Padding(4);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(123, 112);
            this.button7.TabIndex = 7;
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button_Click);
            this.button7.MouseLeave += new System.EventHandler(this.button_MouseLeave);
            this.button7.MouseHover += new System.EventHandler(this.button_MouseHover);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button3.Location = new System.Drawing.Point(270, 261);
            this.button3.Margin = new System.Windows.Forms.Padding(4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(123, 112);
            this.button3.TabIndex = 3;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button_Click);
            this.button3.MouseLeave += new System.EventHandler(this.button_MouseLeave);
            this.button3.MouseHover += new System.EventHandler(this.button_MouseHover);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button2.Location = new System.Drawing.Point(139, 261);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(123, 112);
            this.button2.TabIndex = 2;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button_Click);
            this.button2.MouseLeave += new System.EventHandler(this.button_MouseLeave);
            this.button2.MouseHover += new System.EventHandler(this.button_MouseHover);
            // 
            // button9
            // 
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button9.Location = new System.Drawing.Point(270, 21);
            this.button9.Margin = new System.Windows.Forms.Padding(4);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(123, 112);
            this.button9.TabIndex = 9;
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button_Click);
            this.button9.MouseLeave += new System.EventHandler(this.button_MouseLeave);
            this.button9.MouseHover += new System.EventHandler(this.button_MouseHover);
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button8.Location = new System.Drawing.Point(139, 21);
            this.button8.Margin = new System.Windows.Forms.Padding(4);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(123, 112);
            this.button8.TabIndex = 8;
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button_Click);
            this.button8.MouseLeave += new System.EventHandler(this.button_MouseLeave);
            this.button8.MouseHover += new System.EventHandler(this.button_MouseHover);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button1.Location = new System.Drawing.Point(8, 261);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(123, 112);
            this.button1.TabIndex = 1;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button_Click);
            this.button1.MouseLeave += new System.EventHandler(this.button_MouseLeave);
            this.button1.MouseHover += new System.EventHandler(this.button_MouseHover);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.button8);
            this.groupBox1.Controls.Add(this.button9);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.button7);
            this.groupBox1.Controls.Add(this.button4);
            this.groupBox1.Controls.Add(this.button6);
            this.groupBox1.Controls.Add(this.button5);
            this.groupBox1.Location = new System.Drawing.Point(20, 43);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(403, 386);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            // 
            // o_pobjede_lbl
            // 
            this.o_pobjede_lbl.AutoSize = true;
            this.o_pobjede_lbl.Location = new System.Drawing.Point(44, 433);
            this.o_pobjede_lbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.o_pobjede_lbl.Name = "o_pobjede_lbl";
            this.o_pobjede_lbl.Size = new System.Drawing.Size(75, 17);
            this.o_pobjede_lbl.TabIndex = 11;
            this.o_pobjede_lbl.Text = "O Pobjede";
            // 
            // izjednaceno_lbl
            // 
            this.izjednaceno_lbl.AutoSize = true;
            this.izjednaceno_lbl.Location = new System.Drawing.Point(171, 433);
            this.izjednaceno_lbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.izjednaceno_lbl.Name = "izjednaceno_lbl";
            this.izjednaceno_lbl.Size = new System.Drawing.Size(84, 17);
            this.izjednaceno_lbl.TabIndex = 12;
            this.izjednaceno_lbl.Text = "Izjednaceno";
            // 
            // x_pobjede_lbl
            // 
            this.x_pobjede_lbl.AutoSize = true;
            this.x_pobjede_lbl.Location = new System.Drawing.Point(311, 433);
            this.x_pobjede_lbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.x_pobjede_lbl.Name = "x_pobjede_lbl";
            this.x_pobjede_lbl.Size = new System.Drawing.Size(73, 17);
            this.x_pobjede_lbl.TabIndex = 13;
            this.x_pobjede_lbl.Text = "X Pobjede";
            // 
            // o_pobjede
            // 
            this.o_pobjede.AutoSize = true;
            this.o_pobjede.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.o_pobjede.Location = new System.Drawing.Point(72, 465);
            this.o_pobjede.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.o_pobjede.Name = "o_pobjede";
            this.o_pobjede.Size = new System.Drawing.Size(27, 29);
            this.o_pobjede.TabIndex = 14;
            this.o_pobjede.Text = "0";
            // 
            // izjednaceno
            // 
            this.izjednaceno.AutoSize = true;
            this.izjednaceno.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.izjednaceno.Location = new System.Drawing.Point(202, 465);
            this.izjednaceno.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.izjednaceno.Name = "izjednaceno";
            this.izjednaceno.Size = new System.Drawing.Size(27, 29);
            this.izjednaceno.TabIndex = 15;
            this.izjednaceno.Text = "0";
            // 
            // x_pobjede
            // 
            this.x_pobjede.AutoSize = true;
            this.x_pobjede.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.x_pobjede.Location = new System.Drawing.Point(336, 465);
            this.x_pobjede.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.x_pobjede.Name = "x_pobjede";
            this.x_pobjede.Size = new System.Drawing.Size(27, 29);
            this.x_pobjede.TabIndex = 16;
            this.x_pobjede.Text = "0";
            // 
            // newgame
            // 
            this.newgame.Location = new System.Drawing.Point(349, 508);
            this.newgame.Name = "newgame";
            this.newgame.Size = new System.Drawing.Size(77, 48);
            this.newgame.TabIndex = 17;
            this.newgame.Text = "Nova igra";
            this.newgame.UseVisualStyleBackColor = true;
            this.newgame.Click += new System.EventHandler(this.newgame_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(438, 568);
            this.Controls.Add(this.newgame);
            this.Controls.Add(this.x_pobjede);
            this.Controls.Add(this.izjednaceno);
            this.Controls.Add(this.o_pobjede);
            this.Controls.Add(this.x_pobjede_lbl);
            this.Controls.Add(this.izjednaceno_lbl);
            this.Controls.Add(this.o_pobjede_lbl);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label o_pobjede_lbl;
        private System.Windows.Forms.Label x_pobjede_lbl;
        private System.Windows.Forms.Label o_pobjede;
        private System.Windows.Forms.Label izjednaceno_lbl;
        private System.Windows.Forms.Label izjednaceno;
        private System.Windows.Forms.Label x_pobjede;
        private System.Windows.Forms.Button newgame;
    }
}

