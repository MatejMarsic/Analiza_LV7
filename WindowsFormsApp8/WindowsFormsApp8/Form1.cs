﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp8
{
    public partial class Form1 : Form
    {
        bool red;//ako je true X je na redu, ako je false O je na redu
        int brojac_poteza = 0;
        static String igrac1, igrac2;
       

        /*Napravite igru križić-kružić (iks-oks) korištenjem znanja stečenih na ovoj
        laboratorijskoj vježbi. Omogućiti pokretanje igre, unos imena dvaju igrača, ispis
        koji igrač je trenutno na potezu, igranje igre s iscrtavanjem križića i kružića na
        odgovarajućim mjestima te ispis dijaloga s porukom o pobjedi, odnosno
        neriješenom rezultatu kao i praćenje ukupnog rezultata.*/
        public Form1()
        {                           
            InitializeComponent();
            
        }

        public static void postavi_imena(string n1, string n2)
        {
            igrac1 = n1;
            igrac2 = n2;
        }

        private void button_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (red)
            {
                b.Text = "X";
                label1.Text = igrac1 + " je na redu!";

            }
            else
            {
                b.Text = "O";
                label1.Text = igrac2 + " je na redu!";

            }

            red = !red;
            b.Enabled = false;
            brojac_poteza++;
            if(brojac_poteza > 4)
            {
                provjera_pobjednika();
            }
        }

        private void provjera_pobjednika()
        {
            bool ima_pobjednik = false;
            //vodoravna provjera
            if ((button1.Text == button2.Text) && (button2.Text == button3.Text) && (!button1.Enabled))
            {
                ima_pobjednik = true;
            }
            else if ((button4.Text == button5.Text) && (button5.Text == button6.Text) && (!button4.Enabled))
            {
                ima_pobjednik = true;
            }
            else if ((button7.Text == button8.Text) && (button8.Text == button9.Text) && (!button7.Enabled))
            {
                ima_pobjednik = true;
            }
            //okomita provjera
            else if ((button1.Text == button4.Text) && (button4.Text == button7.Text) && (!button1.Enabled))
            {
                ima_pobjednik = true;
            }
            else if ((button2.Text == button5.Text) && (button5.Text == button8.Text) && (!button2.Enabled))
            {
                ima_pobjednik = true;
            }
            else if ((button3.Text == button6.Text) && (button6.Text == button9.Text) && (!button3.Enabled))
            {
                ima_pobjednik = true;
            }
            //dijagonalna provjera
            else if ((button1.Text == button5.Text) && (button5.Text == button9.Text) && (!button1.Enabled))
            {
                ima_pobjednik = true;
            }
            else if ((button3.Text == button5.Text) && (button5.Text == button7.Text) && (!button3.Enabled))
            {
                ima_pobjednik = true;
            }


            if (ima_pobjednik)
            {
                disable_buttons();                
                String pobjednik = "";
                if (red)
                {
                    pobjednik = igrac1;
                    o_pobjede.Text = (int.Parse(o_pobjede.Text + 1)).ToString();

                }
                else
                {
                    pobjednik = igrac2;
                    x_pobjede.Text = (int.Parse(x_pobjede.Text) + 1).ToString();
                }
                MessageBox.Show(pobjednik + " je pobjedio!", "Rezultat");
            }
            else
            {
                if (brojac_poteza == 9)
                {
                    izjednaceno.Text = (int.Parse(izjednaceno.Text) + 1).ToString();
                    MessageBox.Show("Izjednaceno!", "Rezultat");
                }
            }
        } 

        private void newgame_Click(object sender, EventArgs e)
        {
            var Buttons = groupBox1.Controls.OfType<Button>();
            red = true;
            brojac_poteza = 0;

            foreach (Button btn in Buttons)
            {
                
                btn.Enabled = true;
                btn.Text = "";
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Form2 f2 = new Form2();
            f2.ShowDialog();
            label1.Text = igrac1 + " je na redu!"; 
            o_pobjede_lbl.Text = igrac1;
            x_pobjede_lbl.Text = igrac2;
        }

        private void button_MouseHover(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (red)
            {
                b.Text = "X";
            }
            else
            {
                b.Text = "O";
            }
        }

        private void button_MouseLeave(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (b.Enabled)
            {
                b.Text = "";
            }
        }

       

        private void disable_buttons()
        {
            var Buttons = groupBox1.Controls.OfType<Button>();
            foreach (Button btn in Buttons)
            {
                btn.Enabled = false;
            }
        }
    }
}
